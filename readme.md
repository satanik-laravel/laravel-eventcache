# Satanik EventCache Package

[![Software License][ico-license]](LICENSE.md)

This simple package adds the possibility to list to events across requests.

## Structure

The directory structure follows the industry standard.

```bash
config/
src/
tests/
```


## Install

Via Composer

``` bash
$ composer require satanik/eventcache
```

if the package is not published use this in the composer file

```json
"repositories": {
  "satanik/eventcache": {
    "type": "vcs",
    "url": "git@gitlab.com:satanik/laravel-eventcache.git"
  },
  ...
},
...
"require": {
  ...
  "satanik/eventcache": "<version constraint>",
```

or copy the repository to

```bash
<project root>/packages/Satanik/EventCache
```

and use this code in the composer file

```json
"repositories": {
  "satanik/eventcache": {
    "type": "path",
    "url": "packages/Satanik/EventCache",
    "options": {
      "symlink": true
    }
  },
  ...
},
...
"require": {
  ...
  "satanik/eventcache": "@dev",
```

## Usage

### EventCache

The `EventCache` facade provides two functions to list to cached events and fire cached events.

```php
use EventCache;

$success = EventCache::listen('custom_event', function(array $data) {
    
});

EventCache::fire('custom_event', $payload, $halt);
```

## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email daniel@satanik.at instead of using the issue tracker.

## Credits

- [Daniel Satanik][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-author]: https://satanik.at
