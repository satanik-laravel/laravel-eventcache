<?php

namespace Satanik\EventCache;

use Illuminate\Support\ServiceProvider;

class EventCacheServiceProvider extends ServiceProvider
{
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        // Register the service the package provides.
        $this->app->singleton('satanik-eventcache', EventCache::class);
    }
}
