<?php

namespace Satanik\EventCache\Facades;

use Illuminate\Support\Facades\Facade;

class EventCache extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'satanik-eventcache';
    }
}
