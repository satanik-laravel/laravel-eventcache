<?php

namespace Satanik\EventCache;

use Cache;
use Event;

class EventCache
{
    public function __construct()
    {
        if (!Cache::has('satanik.event.listeners')) {
            Cache::forever('satanik.event.listeners', []);
        }

        Event::listen('*', function ($event_name, array $data) {
            if (starts_with($event_name, 'Illuminate\Cache\Events\Cache')) {
                return;
            }

            $listeners = Cache::get('satanik.event.listeners');
            if (!array_key_exists($event_name, $listeners)) {
                return;
            }

            $callback = $listeners[$event_name];
            $callback($data);
            unset($listeners[$event_name]);
            Cache::forever('satanik.event.listeners', $listeners);
        });
    }

    public function listen($event_name, Callable $callback): bool
    {
        if (starts_with($event_name, 'Illuminate\Cache\Events\Cache')) {
            return false;
        }

        $listeners = Cache::get('satanik.event.listeners');

        if (array_key_exists($event_name, $listeners)) {
            return false;
        }

        $listeners[$event_name] = $callback;
        Cache::forever('satanik.event.listeners', $listeners);

        return true;
    }

    public function fire($event, array $payload = array(), $halt = false): void
    {
        Event::fire($event, $payload, $halt);
    }
}
